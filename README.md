# Challenge

Fe-Melov-Aleksandar Challenge-6 Flex_git

Со clone ја корпираме https адресата од креираното репо од gitlab и во работниот фолдер правиме git clone и го пастираме линкот од gitlab. Тоа ни креира фолдер и во него ги ставаме фајловите од проектот. Со pwd проверуваме на која патека сме. Ако сме во работниот фолдер, со cd Challenge влегуваме во фолдерот кој беше креиран со git clone командата. Потоа одиме git checkout -b Challenge-6 за да креираме нова гранка, git add. за да ги додадеме сите фајлови или git add index.html add style.css за поединечно прикачување, git commit -m "initial commit" за да ги комитираме промените направени во фолдерот и git push origin Challenge-6 за да ги прикачеме фајловите на гит.
